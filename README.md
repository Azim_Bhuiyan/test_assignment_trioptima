# README #

This repository consist of a Python service for sending and retrieving text messages.

#### Quick summary ####
The simple Rest web service is implemented using flask in Python. This web service can be consumed Curl command line utility.

##### Resource #####
Everything stored in REST web service data store is a resource. In current context *users* stored in TGS data store is a resource. We can access this resource using URI.

##### Request verbs #####
1. GET: This will be used to get messages from data store of TGS web application.
2. POST: This will be used to send new message in TGS web application.
3. DELETE: This will be used to delete a message from TGS data store.

##### Request Header #####
Extra request sent to server to specify type of response, encoding, and content type and customized parameters. etc.

##### Request Body #####
When trying to send a message, the resource data is sent in body of put request.

##### Response Body #####
These coded returned with the response, and signifies the status of the request sent to the server. These similar to HTTP response code. E.g. 200 OK denotes request succeeded, 404 NOT FOUND denotes resource not found on server.

### How do I get set up? ###
Our resource in this service will be users, it will store all the users published on TGS, in following format

* user_name
* messages
    * sender
    * fetched
    * date
    * message

We will expose REST endpoints to add, get, and delete the messages. CRUD functionality over REST.

#### Summary of set up ####
This service uses Python 3. Before running the service, please make sure to install the foloowing dependencies

* flask
    * `pip3 install flask`
* flask_restful
    * `pip3 install flask_restful`

Then from the repository root, start the python glass REST server using Curl utility

`python3 messages.py`

##### 1. Submit a message to a defined recipient #####
`curl -XPOST http://127.0.0.1:8080/user_name/john1 -H "Content-Type: application/json"  --data '{ "message": "Lorem ipsum dolor sit amet"}'`

##### 2. Fetch previously not fetched messages #####
`curl -XGET http://127.0.0.1:8080/user_name/john1`

##### 3. Delete all messages of the provided user #####
`curl -XDELETE http://127.0.0.1:8080/user_name/john1`




