import datetime

from flask import Flask
from flask_restful import Resource, reqparse, Api

TGS = Flask(__name__)
api = Api(TGS)

# prepopulated resource
users = [
    {
        "user_name": "john1",
        "messages": [
            {
                "sender": "dev",
                "fetched": "true",
                "date": "2020-05-01",
                "message": "lorem ipsum"
            },
            {
                "sender": "cara",
                "fetched": "false",
                "date": "2020-08-01",
                "message": "lorem ipsum lorem ipsum lorem ipsum lorem ipsum"
            }
        ]
    },
    {
        "user_name": "dev",
        "messages": [
            {
                "sender": "cara",
                "fetched": "true",
                "date": "2020-07-01",
                "message": "lorem ipsum lorem ipsum lorem ipsum"
            },
            {
                "sender": "john1",
                "fetched": "false",
                "date": "2020-07-15",
                "message": "lorem ipsum lorem ipsum lorem ipsum"
            }
        ]
    },
    {
        "user_name": "cara",
        "messages": [
            {
                "sender": "john1",
                "fetched": "false",
                "date": "2020-01-01",
                "message": "lorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsum"
            },
            {
                "sender": "dev",
                "fetched": "false",
                "date": "2020-05-01",
                "message": "lorem ipsumlorem ips lorem ipsumlorem ips"
            }
        ]
    }
]

class TextMessage(Resource):
    def get(self, user_name):
        message_list = list()
        for user in users:
            if(user["user_name"] == user_name):
                for message in user["messages"]:
                    if(message["fetched"] == "false"):
                        message_list.append(message.copy())
                        message["fetched"] = "true"
                if not message_list:
                    return "no new message to fetch", 404
                return message_list, 200
        return "user not found", 404

    def post(self, user_name):
        parser = reqparse.RequestParser()
        parser.add_argument("message")
        args = parser.parse_args()

        message = {
            "sender": "user",
            "date": str(datetime.datetime.now().date()),
            "fetched": "false",
            "message": args["message"]
        }

        for user in users:
            if(user["user_name"] == user_name):
                print(type(user["messages"]))
                user["messages"].append(message)
                return  user, 201
        entry = dict()
        entry["user_name"] = user_name
        entry["messages"] = [message]
        users.append(entry)
        return entry, 201

    def delete(self, user_name):
        for user in users:
            if(user["user_name"] == user_name):
                users.remove(user)
        return "user is deleted.", 200

api.add_resource(TextMessage, "/user_name/<string:user_name>")
TGS.run(debug=True,port=8080)